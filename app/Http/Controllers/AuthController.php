<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{

    public function auth(Request $request)
    {
        if(!Auth::attempt($request->only('username', 'password')))
        {
            return response()->json([

                'meta' => [
                    'success' => false,
                    'errors'  => ["Password incorrect for {$request->username}"]
                ]

            ], 401);
        }

        $user  = User::where('username', $request['username'])->firstOrFail();
        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([

            'meta' => [
                'success' => true,
                'errors'  => []
            ],

            "data" => [
                "token"             => $token,
                "minutes_to_expire" => 60 * 24
            ]

        ]);

    }


}
