<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeadsPostRequest;
use App\Http\Resources\LeadsResource;
use Illuminate\Http\Request;
use App\Repository\LeadRepositoryInterface;

class LeadsController extends Controller
{

    private LeadRepositoryInterface $leadRepository;

    public function __construct(LeadRepositoryInterface $leadRepository)
    {
        $this->leadRepository = $leadRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $userType = $request->user()->user_type;
        $userId   = $request->user()->id;

        return $this->leadRepository->getAllLeads($userType, $userId);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(LeadsPostRequest $request)
    {

        $data  = $request;
        $leads =  $this->leadRepository->createLead($data);
        return $leads;

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Leads  $leads
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $id   = $request->id;
        $lead = $this->leadRepository->getLeadById($id);
        return $lead;

    }

}
