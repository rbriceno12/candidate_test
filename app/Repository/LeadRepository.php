<?php

namespace App\Repository;

use App\Http\Resources\LeadsResource;
use App\Models\Leads;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Validator;

class LeadRepository implements LeadRepositoryInterface
{

    function createLead($data)
    {
        $userType = $data->user()->user_type;

        if($userType == 'manager'){

            $name   = $data->name;
            $source = $data->source;
            $owner  = $data->owner;

            $lead   = Leads::create([
                'name'   => $name,
                'source' => $source,
                'owner'  => $owner
            ]);

            return response()->json([

                'meta' => [
                    'success' => true,
                    'errors'  => []
                ],

                'data' => $lead

            ]);

        }else{
            return response()->json([
                'meta' => [
                    'success' => false,
                    'errors'  => ['No permissions for this action, you should be a manager']
                ]
            ], 401);
        }
    }

    function getAllLeads($userType, $userId)
    {
        if($userType == 'manager')
            $leads = Leads::all();
        else
            $leads = Leads::where('owner', $userId)->get();

        return LeadsResource::collection($leads);
    }

    function getLeadById($id)
    {
        try {

            $lead  = Leads::where('id', $id)->firstOrFail();

            return response()->json([

                'meta' => [
                    'success' => true,
                    'errors'  => []
                ],

                'data' => $lead

            ]);

        } catch (ModelNotFoundException $e) {
            return response()->json([
                'meta' => [
                    'success' => false,
                    'errors'  => ['No lead found']
                ]
            ], 404);
        }
    }
}
