<?php

namespace App\Repository;

interface LeadRepositoryInterface
{
    function createLead($data);
    function getAllLeads($userType, $userId);
    function getLeadById($id);
}
