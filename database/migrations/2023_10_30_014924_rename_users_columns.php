<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameUsersColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->renameColumn('email', 'username');
            $table->renameColumn('email_verified_at', 'username_verified_at');
        });
    }


    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->renameColumn('username','email');
            $table->renameColumn('username_verified_at', 'email_verified_at');
        });
    }
}
